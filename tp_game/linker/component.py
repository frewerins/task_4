from abc import ABC, abstractmethod
from units.magic_unit import MagicUnit


class Component(MagicUnit):
    @property
    def parent(self, parent):
        return self._parent

    @property.setter
    def parent(self, parent: Component):
        self._parent = parent

    def add(self, component: Component):
        pass

    def remove(self, component: Component):
        pass

    def is_composite(self):
        return False

    @abstractmethod
    def move(self, x, y):
        pass

    @abstractmethod
    def info_health(self):
        pass

    @abstractmethod
    def damage(self):
        pass

    @abstractmethod
    def treat(self, points):
        pass


class Leaf(Component):
    def move(self, x, y):
        self._x += x
        self._y += y

    def info_health(self):
        return self._health

    def damage(self, loss):
        self._health -= loss

    def treat(self, points):
        self._health += points


class Composite(Component):
    def __init__(self):
        self._children: list[Component] = []

    def add(self, component: Component):
        self._children.append(component)
        component.parent(self)

    def remove(self, component: Component):
        self._children.remove(component)
        component.parent = None

    def is_composite(self):
        return True

    def move(self, x, y):
        for child in self._children:
            child.move(x, y)

    def info_health(self):
        self._health = 0
        for child in self._children:
            self._health += child.info_health()
        self._health = (self._health * 100) / len(self._children)
        return self._health

    def damage(self, loss):
        for child in self._children:
            child.damage(loss)

    def treat(self, points):
        for child in self._children:
            child.treat(points)


