from abc import ABC, abstractmethod
from design import const

class MagicUnit(ABC):
    health = 100
    damage = None
    speed = None
    protection = None
    price = None
    energy = 100
    _x = const.start_x
    _y = const.start_y

    @abstractmethod
    def attack(self):
        pass

    @abstractmethod
    def treat(self):
        pass

    @abstractmethod
    def shield(self):
        pass
